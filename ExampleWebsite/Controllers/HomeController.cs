﻿using System;
using System.Web.Mvc;
using ExampleWebSite.Models;

namespace ExampleWebSite.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View(new HomeModel());
        }

        public ActionResult Date()
        {
            return PartialView(new DatepickerModel { Date = DateTime.Now });
        }
    }
}