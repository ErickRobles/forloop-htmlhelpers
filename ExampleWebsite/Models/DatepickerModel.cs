﻿using System;

namespace ExampleWebSite.Models
{
    public class DatepickerModel
    {
        public DateTime Date { get; set; }
    }
}