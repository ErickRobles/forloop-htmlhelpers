using System.Web;
using Moq;

namespace Forloop.UnitTests
{
    public static class MockHttpRequestBaseExtensions
    {
        public static void IsAjaxRequest(this Mock<HttpRequestBase> request)
        {
            request.Setup(x => x["X-Requested-With"]).Returns("XMLHttpRequest");
        }
    }
}