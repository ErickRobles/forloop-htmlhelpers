using System;
using System.Text;
using System.Web;
using System.Web.WebPages;

namespace Forloop.UnitTests
{
    public static class TestExtensions
    {
        public static string BuildScriptElementWithSrc(this string path)
        {
            return string.Format("<script type='text/javascript' src='{0}'></script>", path);
        }

        public static string BuildScriptElementWithTag(this string block)
        {
            return string.Format("<script type='text/javascript'>{0}</script>", block);
        }

        public static string BuildScriptTemplate(this string block)
        {
            var builder = new StringBuilder();
            builder.AppendLine(block);
            return builder.ToString();
        }

        public static string BuildScriptTemplate(this string[] blocks)
        {
            var builder = new StringBuilder();

            foreach (var block in blocks)
            {
                builder.AppendLine(block);
            }

            return builder.ToString();
        }

        public static Func<dynamic, HelperResult> ScriptTemplate(this string script)
        {
            return o => new HelperResult(writer => writer.Write(script));
        }

        public static string BuildScriptElementsWithTag(this string[] blocks)
        {
            var builder = new StringBuilder();

            foreach (var block in blocks)
            {
                builder.AppendLine(block.BuildScriptElementWithTag());
            }

            return builder.ToString();
        }

        public static IHtmlString BuildScriptElementsWithSrc(this string[] paths)
        {
            var builder = new StringBuilder(paths.Length);
            foreach (var path in paths)
            {
                builder.AppendLine(BuildScriptElementWithSrc(path));
            }

            return new HtmlString(builder.ToString());
        }


    }
}